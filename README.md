# ServerCraft

Is the `server.properties` file too overwhelming? Here is a graphical application to help you out with that!

## About

ServerCraft is designed for the layman who has knowledge of Minecraft but has no idea how to create a simple server. It provides a simple interface for running a Minecraft server and managing it's settings, all automated with simple clicks.

## Features

* Utilizes the Spigot Minecraft server for performance and extensibility
* Automatically downloads the correct Java version, no more confusing errors when trying to run a server
* Common settings available at a glance
* Separate section for common settings that are a little more advanced, such as:
  * The amount of RAM to allocate to the server
  * Render and simulation distances
  * Enabling command blocks

## Screenshots

provide screenshots

## Building and Running

Pre-compiled binaries are available to download [here](https://codeberg.org/brysonsteck/ServerCraft/releases). Simply download the correct file for your OS. (`.exe` or `.msi` for Windows, `.dmg` or `.pkg` for macOS, etc.)

ServerCraft uses Gradle as the build system of choice. At the very least, you will need to install at least Java 11. ServerCraft is built on OpenJFX, of which requires Java 11 or later. The binaries I offer on the [releases page](https://codeberg.org/brysonsteck/ServerCraft/releases) are bundled with Java 17 and do not require installing Java to run.

However, installing Gradle is not necessary. You can replace `gradle` in the following instructions with `./gradlew` on Unix systems and `.\gradlew.bat` on Windows systems, as long as your current directory is the root of the repository. These scripts download and run a Gradle jar file.

To build ServerCraft from source, creating a fat jar, Java class files, etc., run:
```
gradle build
```

To run ServerCraft from source, run:
```
gradle run
```

To compile binaries for the current system, run:
```
gradle pack
```
**NOTE**: Building binaries on Windows requires the WiX toolset and Inno Setup to be installed and on the PATH. See this JavaPackager [GitHub page](https://github.com/fvarrui/JavaPackager/blob/master/docs/windows-tools-guide.md) for installation instructions.