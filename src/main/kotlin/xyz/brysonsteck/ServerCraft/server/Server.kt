package xyz.brysonsteck.ServerCraft.server

import java.io.File
import java.io.FileNotFoundException
import java.util.Properties

public class Server {
  public var jar = ""
  public var dir = ""

  private val props = Properties()

  public fun loadProps() {
    props.setProperty("allow-flight", false.toString())
    props.setProperty("allow-nether", true.toString())
    props.setProperty("generate-structures", true.toString())
    props.setProperty("hardcore", false.toString())
    props.setProperty("pvp", true.toString())
    props.setProperty("white-list", false.toString())
    props.setProperty("enable-command-block", false.toString())
    props.setProperty("hide-online-players", false.toString())
    props.setProperty("max-players", 20.toString())
    props.setProperty("max-world-size", 29999984.toString())
    props.setProperty("server-port", 25565.toString())
    props.setProperty("view-distance", 10.toString())
    props.setProperty("jvm-ram", 1024.toString())
    props.setProperty("spawn-protection", 16.toString())
    props.setProperty("simulation-distance", 10.toString())
    props.setProperty("max-tick-time", 60000.toString())
    props.setProperty("difficulty", "normal")
    props.setProperty("gamemode", "survival")
    props.setProperty("level-name", "world")
    props.setProperty("level-seed", "")
    props.setProperty("level-type", "minecraft:normal")
    props.setProperty("motd", "A server for a dummy")
    writeProps()
  }

  public fun loadProps(dir: String, convert: Boolean = false) {
    var ins = File(dir + File.separator + "server.properties").inputStream()
    props.load(ins)
    try {
      ins =
              File(dir + File.separator + "ServerCraft" + File.separator + "ServerCraft.properties")
                      .inputStream()
      props.load(ins)
    } catch (e: FileNotFoundException) {
      if (convert) {
        // create the file in question, as this is an external server being converted into a
        // ServerCraft managed one
        File(dir + File.separator + "ServerCraft" + File.separator + "ServerCraft.properties")
                .createNewFile()
        // also apply app-specific properties
        props.setProperty("jvm-ram", 1024.toString())
        // then write to file
        val temp = Properties()
        val outs =
                File(
                                dir +
                                        File.separator +
                                        "ServerCraft" +
                                        File.separator +
                                        "ServerCraft.properties"
                        )
                        .outputStream()
        temp.setProperty("jvm-ram", props.getProperty("jvm-ram"))
        temp.store(
                outs,
                "ServerCraft settings backup\nSometimes, Minecraft will completely overwrite the server.properties file,\ncompletely destroying these app-specific settings. This file backs up these settings\njust in case. :)\nhttps://codeberg.org/brysonsteck/ServerCraft"
        )
      } else {
        println(e)
      }
    }
  }

  private fun writeProps() {
    var outs = File(dir + File.separator + "server.properties").outputStream()
    props.store(
            outs,
            "Minecraft server properties\nCreated with ServerCraft: https://codeberg.org/brysonsteck/ServerCraft"
    )
    val temp = Properties()
    outs =
            File(dir + File.separator + "ServerCraft" + File.separator + "ServerCraft.properties")
                    .outputStream()
    temp.setProperty("jvm-ram", props.getProperty("jvm-ram"))
    temp.store(
            outs,
            "ServerCraft settings backup\nSometimes, Minecraft will completely overwrite the server.properties file,\ncompletely destroying these app-specific settings. This file backs up these settings\njust in case. :)\nhttps://codeberg.org/brysonsteck/ServerCraft"
    )
  }

  public fun getProp(prop: String): String {
    return props.getProperty(prop)
  }

  public fun setProp(key: String, value: Any) {
    props.setProperty(key, value.toString())
    writeProps()
  }
}
