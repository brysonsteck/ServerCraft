package xyz.brysonsteck.ServerCraft.controllers

import java.awt.Desktop
import java.net.URI
import java.util.Properties
import javafx.event.ActionEvent
import javafx.fxml.FXML
import javafx.scene.Node
import javafx.scene.control.Hyperlink
import javafx.scene.control.Label
import javafx.stage.Stage
import xyz.brysonsteck.ServerCraft.App

class InfoController {
  @FXML lateinit private var version: Label

  private val emails = mapOf("bryson" to "me@brysonsteck.xyz")
  private val websites = mapOf("bryson" to "https://brysonsteck.xyz")
  private val source = "https://codeberg.org/brysonsteck/ServerCraft"
  private val license = "https://www.gnu.org/licenses/gpl-3.0.html"

  @FXML
  public fun initialize() {
    val props = Properties()
    props.load(App().javaClass.getResourceAsStream("info.properties"))
    version.text += props.getProperty("version")
  }

  @FXML
  private fun openHyperlink(e: ActionEvent) {
    val link = e.source as Hyperlink
    link.isVisited = false
    val split = link.id.split('_').toMutableList()
    split.add("")
    val os = System.getProperty("os.name").lowercase()

    val desktop = Desktop.getDesktop()
    try {
      when {
        split[1].equals("email") -> {
          if (!os.contains("linux")) {
            desktop.browse(URI("mailto:" + emails[split[0]]))
          } else {
            Runtime.getRuntime().exec("xdg-open mailto:" + emails[split[0]])
          }
        }
        split[1].equals("website") -> {
          if (!os.contains("linux")) {
            desktop.browse(URI(websites[split[0]]))
          } else {
            Runtime.getRuntime().exec("xdg-open " + websites[split[0]])
          }
        }
        split[0].equals("source") -> {
          if (!os.contains("linux")) {
            desktop.browse(URI(source))
          } else {
            Runtime.getRuntime().exec("xdg-open " + source)
          }
        }
        split[0].equals("license") -> {
          if (!os.contains("linux")) {
            desktop.browse(URI(license))
          } else {
            Runtime.getRuntime().exec("xdg-open " + license)
          }
        }
      }
    } catch (e: Exception) {
      println(e)
    }
  }

  @FXML
  private fun closeInfo(e: ActionEvent) {
    val source = e.getSource() as Node
    val stage = source.getScene().getWindow() as Stage
    stage.close()
  }
}
