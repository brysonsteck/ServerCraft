package xyz.brysonsteck.ServerCraft

import javafx.application.Application
import javafx.fxml.FXMLLoader
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.scene.image.Image
import javafx.stage.Stage

class App : Application() {

  override fun start(stage: Stage) {
    var scene = Scene(loadFXML("primary"), 963.0, 713.0)
    // Application.setUserAgentStylesheet(CupertinoLight().getUserAgentStylesheet())
    scene.stylesheets.add(this.javaClass.getResource("css/style.css").toExternalForm())
    stage.icons.add(Image(this.javaClass.getResourceAsStream("app.png")))
    stage.setResizable(true)
    stage.title = "ServerCraft"
    stage.scene = scene
    stage.show()
  }

  public fun loadFXML(fxml: String): Parent {
    val fxmlLoader = FXMLLoader(this.javaClass.getResource(fxml + ".fxml"))
    return fxmlLoader.load()
  }

  public fun run() {
    launch()
  }
}
