package xyz.brysonsteck.ServerCraft.dialogs

import javafx.geometry.Insets
import javafx.scene.Scene
import javafx.scene.control.Button
import javafx.scene.control.ButtonBar
import javafx.scene.control.Label
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.scene.layout.Pane
import javafx.stage.Modality
import javafx.stage.Stage
import xyz.brysonsteck.ServerCraft.App

abstract class Dialog {
  public var result = false

  abstract val type: String
  abstract val title: String
  abstract val msg: String
  abstract val neutralButton: Button?
  abstract val yesButton: Button?
  abstract val noButton: Button?

  protected var hold: Boolean = true
  protected var labelOffset: Double = 40.0
  protected val dialog = Stage()

  public fun show(): Boolean {
    val resources = App().javaClass.getResource("icons/$type.png")
    val icon = Image("$resources")
    dialog.icons.add(Image(App().javaClass.getResourceAsStream("app.png")))
    dialog.setResizable(false)
    dialog.initModality(Modality.APPLICATION_MODAL)
    dialog.title = title
    val imagePane = Pane()
    imagePane.layoutX = 14.0
    imagePane.layoutY = 14.0
    imagePane.scaleX = 0.7
    imagePane.scaleY = 0.7
    imagePane.children.add(ImageView(icon))
    val label = Label(msg)
    label.isWrapText = true
    label.layoutX = 115.0
    label.layoutY = labelOffset
    val buttonBar = ButtonBar()
    buttonBar.padding = Insets(10.0)
    buttonBar.layoutX = 0.0
    buttonBar.layoutY = 107.0
    buttonBar.prefWidth = 400.0
    val scenePane = Pane()
    scenePane.children.addAll(imagePane, label, buttonBar)
    if (noButton != null) {
      ButtonBar.setButtonData(noButton, ButtonBar.ButtonData.RIGHT)
      buttonBar.buttons.add(noButton)
    }
    if (yesButton != null) {
      ButtonBar.setButtonData(yesButton, ButtonBar.ButtonData.RIGHT)
      buttonBar.buttons.add(yesButton)
    }
    if (neutralButton != null) {
      ButtonBar.setButtonData(neutralButton, ButtonBar.ButtonData.LEFT)
      buttonBar.buttons.add(neutralButton)
    }
    val dialogScene = Scene(scenePane, 400.0, 150.0)
    dialog.setScene(dialogScene)
    if (hold) {
      dialog.showAndWait()
    } else {
      dialog.show()
    }
    return result
  }
}
