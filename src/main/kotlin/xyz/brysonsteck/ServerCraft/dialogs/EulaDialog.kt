package xyz.brysonsteck.ServerCraft.dialogs

import java.awt.Desktop
import java.net.URI
import javafx.event.ActionEvent
import javafx.event.EventHandler
import javafx.scene.control.Button

class EulaDialog : Dialog {
  override val type: String
  override val title: String
  override val msg: String
  override val neutralButton: Button?
  override val yesButton: Button?
  override val noButton: Button?

  constructor() {
    type = "warning"
    title = "Minecraft End User License Agreement (EULA)"
    msg = "Do you agree to the terms of the Minecraft\nEnd User License Agreement?"

    noButton = Button("I Disagree")
    noButton.onAction =
            EventHandler<ActionEvent>() {
              result = false
              dialog.hide()
            }
    noButton.isDefaultButton = true
    yesButton = Button("I Agree")
    yesButton.onAction =
            EventHandler<ActionEvent>() {
              result = true
              dialog.hide()
            }
    neutralButton = Button("View EULA")
    neutralButton.onAction =
            EventHandler<ActionEvent>() {
              val desktop = Desktop.getDesktop()
              if (desktop.isSupported(Desktop.Action.BROWSE)) {
                // most likely running on Windows or macOS
                try {
                  desktop.browse(URI("https://www.minecraft.net/en-us/eula"))
                } catch (e: Exception) {
                  println(e)
                }
              } else {
                // assume running on linux
                try {
                  Runtime.getRuntime().exec("xdg-open https://www.minecraft.net/en-us/eula")
                } catch (e: Exception) {
                  println(e)
                }
              }
            }
  }
}
