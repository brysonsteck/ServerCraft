package xyz.brysonsteck.ServerCraft.dialogs

import javafx.event.ActionEvent
import javafx.event.EventHandler
import javafx.scene.control.Button

class CommonDialog : Dialog {
  override var type: String
  override var title: String
  override var msg: String
  override val neutralButton: Button?
  override val yesButton: Button?
  override val noButton: Button?

  constructor(type: String, title: String, msg: String) {
    this.type = type
    this.title = title
    this.msg = msg

    noButton = Button("No")
    noButton.onAction =
            EventHandler<ActionEvent>() {
              result = false
              dialog.hide()
            }
    yesButton = Button("Yes")
    yesButton.onAction =
            EventHandler<ActionEvent>() {
              result = true
              dialog.hide()
            }
    yesButton.isDefaultButton = true
    neutralButton = null
  }
}
