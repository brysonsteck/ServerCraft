package xyz.brysonsteck.ServerCraft.dialogs

import javafx.event.ActionEvent
import javafx.event.EventHandler
import javafx.scene.control.Button

class KillDialog : Dialog {
  override val type: String
  override val title: String
  override val msg: String
  override val neutralButton: Button?
  override val yesButton: Button?
  override val noButton: Button?

  constructor() {
    type = "warning"
    title = "Server is still running!"
    msg =
            "You should only kill the server if absolutely necessary, i.e. not being responsive after long periods of time. Data loss may occur. Continue anyway?"
    hold = false

    noButton = Button("No")
    noButton.onAction =
            EventHandler<ActionEvent>() {
              result = false
              dialog.hide()
            }
    yesButton = Button("Yes")
    yesButton.onAction =
            EventHandler<ActionEvent>() {
              result = true
              dialog.hide()
            }
    yesButton.isDefaultButton = true
    neutralButton = null
  }
}
