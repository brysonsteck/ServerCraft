# Features

Here are all the features currently implemented/planned for ServerCraft:

* [X] Change `server.properties` file
* [X] Adapt controls based on what properties are found
* [X] Save server-specific settings in subdirectory
* [X] Allow changing memory passed into JVM
* [ ] Manage port forwarding automatically using UPnP
* [ ] Create system to check for updates
* [ ] Add and remove Spigot server mods/plugins
* [ ] Create servers of any Minecraft version >= 1.8
* [ ] "Advanced mode" with all settings editable
* [ ] Recognize existing server jars and give option to run them instead
